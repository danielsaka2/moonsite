import {createStackNavigator, createAppContainer } from 'react-navigation'
import home from './src/screens/home/home'
import show from './src/screens/show/show'
const AppStack = createStackNavigator({
 HomeScreen:{
   screen:home,
   navigationOptions: () => ({
    headerStyle:{
      backgroundColor: '#999999'
    }
  })
 },
 ShowScreen:{
  screen:show,
  navigationOptions: () => ({
    headerStyle:{
      backgroundColor: '#999999'
    }
  })
},
});
const AppContainer = createAppContainer(AppStack);
export default AppContainer;
