import React from 'react';
import { Image, View, Text, StyleSheet, ScrollView ,WebView} from 'react-native';
import noPreview from '../../../assests/NoPreview.jpg';


class ShowScreen extends React.Component {
    _handleText=(item,name)=>{
        return(<View>
            <Text style={styles.titleText}>{name}: </Text>
            <Text style={styles.text}>{item}</Text>
        </View>)
    }
    _handleImage=(Image)=>{
        if(Image==null){
            return false;
        }
        else{
            return true;
        }
    }
    render(){
        const { navigation } = this.props;
        const item = navigation.getParam('item');
        const regex = /(<([^>]+)>)/ig;
        imageUrl= item.show.image;
        averageRating=item.show.rating.average;
        summary= item.show.summary.replace(regex, '')
        network= item.show.network
        genres= item.show.genres.toString()
        scheduleTime= item.show.schedule.time
        scheduleDay= item.show.schedule.days.toString()
        language=item.show.language
        return(
            <View style={styles.mainStyle}>
            <View style={styles.scrollview}>
            <ScrollView>
            {this._handleImage(imageUrl)?  
            <Image
                style={styles.image}    
                source={{uri:imageUrl.medium}}
            />
                :<Image
                style={styles.image}    
                source={noPreview}
            />}
                {averageRating?  this._handleText(averageRating,"Average Rating"):null }
                {summary? this._handleText(summary,"Summary") :null }
                {network? this._handleText(network.name,"Network")  : null}
                {genres[0]? this._handleText(genres,"Genres")  : null} 
                {scheduleTime? this._handleText(scheduleTime,"Time") :null} 
                {scheduleDay? this._handleText(scheduleDay,"Day") : null} 
                {language? this._handleText(language,"Language") : null} 
                </ScrollView>
                </View>
                <View style={styles.bottomStyle}>
                    <Text style={styles.bottomText}>Daniel Morad Saka</Text>
                </View>
            </View>
        )
    }
};
const styles=StyleSheet.create({
    mainStyle:{
        flex:1,
    },
    bottomStyle: {
        bottom: 0,
        position: 'absolute',
        width: '100%',
        alignItems: 'center',
        backgroundColor: '#999999'
    },
    bottomText:{
        color:'black',
    },
    image:{
        height:350,
        width: '100%',
        resizeMode:'stretch'
       },
    text:{
        fontSize: 20,
    },
    scrollview:{
        height:'95%',
    },
    titleText: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    item:{
        alignItems: 'center',
        marginBottom: '10%',  
    },
});
export default ShowScreen;