import React from 'react'
import { View, Text, StyleSheet, FlatList ,Image,TouchableOpacity } from 'react-native'
import noPreview from '../../../assests/NoPreview.jpg';
class MainScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
          
    }

    componentDidMount() {
    return fetch('http://api.tvmaze.com/search/shows?q=all')
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          dataSource: responseJson,
        }, 
        );

      })
      .catch((error) =>{
        console.error(error);
      });
    }
    _handleImage=(Image)=>{
        if(Image==null){
            return false;
        }
        else{
            return true;
        }
    }
    _renderItem = ({item}) => (
        imageUrl= item.show.image,
        averageRating=item.show.rating.average,
        <TouchableOpacity onPress={()=>this.props.navigation.navigate('ShowScreen',{item: item})}>
        <View style={styles.item}>
            {this._handleImage(imageUrl)?  
            <Image
                style={styles.image}    
                source={{uri:imageUrl.medium}}
            />
                :<Image
                style={styles.image}    
                source={noPreview}
            />}
            <Text style={styles.text}>{item.show.name}</Text>
            {averageRating? <Text style={styles.text}>average rating: {averageRating}</Text>:null}
            </View>
            </TouchableOpacity>
        )
       
       
    render() {
            
          return (
            <View style= {styles.mainStyle}>
                <View style={styles.flatlist}>
                <FlatList 
                    data={this.state.dataSource}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => index.toString()}
                    />
                </View>
                <View style={styles.bottomStyle}>
                    <Text style={styles.bottomText}>Daniel Morad Saka</Text>
                </View>
            </View>
        )
    }
}; 
const styles = StyleSheet.create({
    mainStyle: {
        flex: 1,
    },
    bottomStyle: {
        bottom: 0,
        position: 'absolute',
        width: '100%',
        alignItems: 'center',
        backgroundColor: '#999999'
    },
    bottomText:{
        color:'black',
    },
    flatlist:{
        height:'90%',
    },
    item:{
        alignItems: 'center',
        marginBottom: '10%',  
    },
    image:{
        height:300,
        width:300,
        resizeMode:'stretch'
    },
    text:{
        fontSize: 20,
    }
});
export default MainScreen;